# ex-typeform-surveys

Keboola Connection docker app for fetching responses from Typeform Responses API. Available under `kds-team.ex-typeform-surveys`

## Functionality
This component allows KBC to fetch responses to Typeform questionnaires.

## Parameters

There are currently three options in the UI:  
- **Personal access token** - https://developer.typeform.com/get-started/personal-access-token/  
- **ID of the questionnaire** - can be found in the URL of the questionnaire  
- **How many days back you want to go** - denotes the start of the time interval you fetch responses from.

## Outcome
Outcome
The component returns two tables. The first one is called NVP_questions and contains three columns:  
- **id** (ID of the question)  
- **title** (Title of the question)  
- **date** (Exact timestamp when the question was extracted)

The second one is called NVP_answers_applicants and contains five columns:  
- **id** (URL from which the applicant came)  
- **field_id** (ID of the question. Pairs with the id column of the NVP_questions table.)  
- **ans_concat** (Answer)  
- **time_submitted** (Time when the applicant submitted the questionnaire)  
- **id2** (Internal ID of the applicant assigned by Typeform)
