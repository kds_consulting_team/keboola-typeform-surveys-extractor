import pandas as pd
import requests
from pandas.io.json import json_normalize
import numpy as np
from keboola import docker
from datetime import datetime, timedelta
import logging
import sys

if not sys.warnoptions:
    import warnings
    warnings.simplefilter("ignore")

sys.tracebacklimit = 0

# Logging
logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(message)s',
    datefmt="%Y-%m-%d %H:%M:%S")

cfg = docker.Config('/data/')

try:
    token = cfg.get_parameters()['#token']
    form_id = cfg.get_parameters()['form_id']
    dayspan = cfg.get_parameters()['dayspan']

    if not int(dayspan) >= 1:
        logging.info('Please use value larger or equal to 1 in the\
             `How many days back you want to go?` parameter.')
        sys.exit(1)

    since = (datetime.utcnow() - timedelta(days=int(dayspan)))\
        .isoformat()
    params = {'since': since
              }
except:  # noqa
    logging.info('Check the inputs, please!')
    sys.exit(1)

headers = {
    'Authorization': 'bearer %s' % token,
    'User-Agent': 'Mozilla/5.0'
}

# url and the request

url_responses = 'https://api.typeform.com/forms/' + \
    form_id + '/responses?page_size=1000'
resp = requests.get(url=url_responses, params=params, headers=headers)

if resp.status_code != 200:
    if resp.status_code == 401 or resp.status_code == 403:
        logging.error('The response from the API is: ' + str(resp.status_code))
        logging.info(
            'Please check the form ID and your access token please.\
             Either your access token is incorrect, or you don\'t have access to this responses ID')
        sys.exit(1)
    else:
        logging.error('The response from the API is: ' + str(resp.status_code))
        if resp.status_code == 404:
            logging.info('We haven\'t found a survey with this ID, please check the form ID.')
        sys.exit(1)

responses = resp.json()['items']

if resp.json()['page_count'] > 1:
    for i in range(resp.json()['page_count'] + 100):
        before_token = resp.json()['items'][-1]['token']
        params = {'since': since,
                  'before': before_token
                  }
        resp = requests.get(url=url_responses, params=params, headers=headers)
        responses.extend(resp.json()['items'])

        if resp.json()['page_count'] == 1:
            print('Everything looped')
            break

no_n_responses = len(responses)
logging.info('The number of new responses is: ' + str(no_n_responses))

url_questions = 'https://api.typeform.com/forms/' + form_id
resp_questions = requests.get(
    url=url_questions, params=params, headers=headers)


# flattening the response
results_df = pd.DataFrame(np.zeros((0, 0)))
answers_df = pd.DataFrame(np.zeros((0, 0)))
questions_df = pd.DataFrame(np.zeros((0, 0)))

if no_n_responses > 0:
    for i in range(len(responses)):
        logging.info('Retrieving response #' + str(i))
        # id of the applicant + time_submitted
        id_applicant = responses[i]['metadata']['referer']
        time_submitted = responses[i]['submitted_at']
        id_applicant2 = responses[i]['landing_id']

        # create df from flattened json
        try:
            response_df = json_normalize(responses[i]['answers'])
            resp_df_cols = response_df.columns

            # all the columns that could be of interest
            all_cols_needed = ['choices.labels', 'email', 'number', 'text',
                               'boolean', 'field.id', 'url', 'choice.label',
                               'file_url', 'date', 'payment']
            try:
                responses[0]['hidden']
                hidden_fields = True
            except KeyError:
                hidden_fields = False

            if hidden_fields:
                hidden_dict = responses[0]['hidden']
                hidden_dict = {k+'_hidden': v for k, v in hidden_dict.items()}
            else:
                pass

            # add the columns to those responses that do not contain them
            for col_name in resp_df_cols:
                response_df[col_name] = '' if (col_name not in all_cols_needed) \
                                        else response_df[col_name]

            # take only the columns of interest, switch NaNs with '' for later concat
            answers = response_df\
                .loc[:, all_cols_needed]\
                .fillna('')

            # cast the columns as strings
            answers['choices.labels'] = answers['choices.labels']\
                .astype(str)
            answers['choice.label'] = answers['choice.label']\
                .astype(str)
            answers['number'] = answers['number']\
                .astype(str)
            answers['boolean'] = answers['boolean']\
                .astype(str)

            # concat the values
            answers['ans_concat'] = answers[['text', 'email', 'number',
                                             'choices.labels', 'boolean',
                                             'url', 'choice.label']]\
                .apply(lambda x: ''.join(x), axis=1)

            # id of the question + value
            results_df_tmp = answers[['field.id', 'ans_concat']]

            if hidden_fields:
                hidden_appendix = pd.DataFrame(list(hidden_dict.items()), columns=[
                    'field.id', 'ans_concat'])
                results_df_tmp = results_df_tmp.append(hidden_appendix)
            else:
                pass

            results_df_tmp = results_df_tmp.set_index('field.id').T
            results_df_tmp['id'] = [id_applicant]
            results_df_tmp['time_submitted'] = [time_submitted]
            results_df_tmp['id2'] = [id_applicant2]

            answers_df_tmp = answers[['field.id', 'ans_concat']]
            if hidden_fields:
                answers_df_tmp = answers_df_tmp.append(hidden_appendix)
            else:
                pass

            answers_df_tmp.insert(0, 'id', id_applicant)
            answers_df_tmp.insert(3, 'time_submitted', time_submitted)
            answers_df_tmp.insert(4, 'id2', id_applicant2)

            # in the first run create the df, in the following just append
            if results_df.empty:
                results_df = results_df_tmp
            else:
                results_df = results_df.append(results_df_tmp)

            if answers_df.empty:
                answers_df = answers_df_tmp
            else:
                answers_df = answers_df.append(answers_df_tmp)

            logging.info('Response #%s retrieved' % str(i))
        except KeyError:
            logging.info(
                'Response #%s not retrieved. No answers provided!' % str(i))
            pass

    for i in range(len(resp_questions.json()['fields'])):
        question = resp_questions.json()['fields'][i]
        question_dict = {
            'id': [question['id']],
            'title': [question['title']],
            'date': [str(datetime.utcnow())]
        }
        questions_df_tmp = pd.DataFrame(data=question_dict)

        if questions_df.empty:
            questions_df = questions_df_tmp
        else:
            questions_df = questions_df.append(questions_df_tmp)


# write the results
    # results_df.to_csv('/data/out/tables/answers_applicants.csv', index=False)
    answers_df.to_csv(
        '/data/out/tables/NVP_answers_applicants.csv', index=False)
    questions_df.to_csv('/data/out/tables/NVP_questions.csv', index=False)

else:
    logging.info('No new responses to fetch.')
